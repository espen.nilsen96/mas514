<!--Transformation between coordinate frames-->

	<node pkg="tf" type="static_transform_publisher" name="map_to_odom" args="0 0 0 0 0 0 1 /map /odom 30"/>
	<node pkg="tf" type="static_transform_publisher" name="baselink_to_camera" args="0.06 0 0.08 0 0 0 /base_link /camera_link 30"/>
	<node pkg="tf" type="static_transform_publisher" name="camera_to_laser" args="0 0 0 0 0 0 /camera_link /laser 30"/>
	<node pkg="tf" type="static_transform_publisher" name="map_to_laser" args="0.06 0 0.08 0 0 0 /map /laser 30" />
 