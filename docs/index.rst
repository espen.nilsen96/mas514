.. MAS507 documentation master file, created by
   sphinx-quickstart on Tue Aug 25 21:04:02 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. image:: ./figs/uia.png
   :width: 600px
   :align: center
   :alt: alternate text

|

.. raw:: html

   <h2 style="text-align: center">University of Agder</h2>
   <p style="text-align: center">Department of Engineering Sciences<br>Grimstad, Norway</p>
   
|

.. image:: ./figs/JetbotReport.jpg
   :width: 12cm
   :align: center

|
|

MAS514 - Robotics and Instrumentation
===================================================

Students
---------

- Espen Nilsen
- Jennie B Andersen

   - Master Mechatronics, students

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   src/Introduction
   src/GettingStarted
   src/ROSNodes
   src/Navigation
   src/ControlJetbotFromExternalComputer
   src/Mapping
   src/RUN



   
   <!--src/WheelEncoder-->
   <!--src/web-controller-->
   <!--src/troubleshooting-->
   <!--src/calibration-->
   <!--src/strawberry-->
   <!--src/test-->


.. Indices and tables
.. ==================



.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
