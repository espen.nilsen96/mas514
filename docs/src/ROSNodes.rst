ROS Nodes
=======================

In order to make the JetBot navigate utilizing ROS, nodes need to be modified and added. This section describes the process.



Transform Coordinate Frames - tf
--------------------------------
The global frame needs to be related to all the local moving frames in the system. Every moving part has its own local frame. In this system the frames are related as illustrated bellow. In addition, there is also a map to laser link as shown in the **start.launch** file.

.. figure:: ../figs/tre.jpg
   :width: 10cm
   :align: center



The following tf nodes are created and placed in the **start.launch** file described in :ref:`Robot Configuration`.

.. literalinclude:: ../figs/tf_nodes.cpp
    :language: c++  




Arduino
------------
The Arduino is used to process the feedback of the JetBots wheel encoders. The following nodes is created using the Arduino with rosserial. Rosserial makes it possible for the Arduino to communicate with ROS over the serial port. Following the installation guide for `Install rosserial_arduino <https://hagenmek.gitlab.io/mas514/src/guides.html#install-rosserial-arduino>`__, including uploading the Odom script, the Arduino is ready to communicate.


Wheel Encoder
................
The wheel encoder publisher should be verified and uploaded to the Arduino.   The `Encoder Signal Publisher Node <https://hagenmek.gitlab.io/mas514/src/guides.html#encoder-signal-publisher-node>`__, as illustrated bellow, communicates the encoder values (wheel angles and number of turns) to ROS.


.. literalinclude:: ../figs/wheelEn.cpp
    :language: c++  

    

Odometry 
...................

Odometry is local motion estimation, it is used to estimate the JetBots change in position over time relative to a known position. The estimation is performed from motion sensor data utilizing the wheel encoders. A node is created for publishing the odometry using Twist. The odometry node defines the JetBot parameters, initial values, global frame position and motion. It is important to use Twist to enable keyboard control. The `How to Publish Odometry using Twist <https://hagenmek.gitlab.io/mas514/src/guides.html#how-to-publish-odometry-using-twist>`__ code is as follows:



.. literalinclude:: ../figs/Odometry_WheelEncoderReader.py
    :language: c++




Base Controller - Inverse Kinematics
.....................................

The Inverse Kinematic Node consists of the `subscribing Twist command <https://hagenmek.gitlab.io/mas514/src/guides.html#how-to-subscribing-twist-command>`__, as shown bellow, which converts the velocity data received from the wheel encoders to motor commands for the mobile JetBot base. The mathematical process is called differential inverse kinematics, determining the corresponding actuator speed based on the desired velocity. This enables the JetBot to move to setpoint destinations. 


.. literalinclude:: ../figs/InverseKinematics.py
    :language: c++




Pointcloud to Laserscan
---------------------
The point cloud to laser scan generates a 2D laser scan from a point cloud based on the provided parameters. The node is initiated in the `sample_node.launch <https://hagenmek.gitlab.io/mas514/src/guides.html#pointclound-to-laserscan>`__ file, as illustrated bellow.


.. literalinclude:: ../figs/sample_node.launch
    :language: c++



Keyboard Control
----------------------

Keyboard control is done by utilizing the Twist codes for communication receiving the velocity data from **cmd_vel**. The implementation is done as follows:

Clone the jetbot Twist keyboard repository:

- ``sudo git clone https://github.com/issaiass/jetbot_twist_keyboard.git``


Test if the keyboard control works:

- ``roscore``
- ``roslaunch mas514 start.launch``
- ``rosrun teleop_twist_keyboard teleop_twist_keyboard.py``

The terminal will display instructions for speed regulation and movement control as follows:

.. literalinclude:: ../figs/key.cpp
    :language: c++





