Requirements
===============================

The requirements include the installation and setup for the JetBot with NVIDIA Jetson Nano, ROS, the Arduino, the wheel encoders, and the Intel RealSense LiDAR L515 camera. The procedure is done following the guides from the `MAS514 main GitLab page <https://hagenmek.gitlab.io/mas514/index.html>`__. The steps were as follows:


- The operating system on the NVIDIA Jetson Nano should be Ubuntu 18.04
- Setup Jetbot following the `Getting Started <https://hagenmek.gitlab.io/mas514/src/start.html>`__ guide
- Install ROS with the `ROS Package <https://hagenmek.gitlab.io/mas514/src/ros.html>`__ guide
- Install and setup Arduino IDE and connect the wheel encoders following `Lab Exercise #3 <https://hagenmek.gitlab.io/mas514/src/labexercise.html#exercise-3>`__ 
- Install and setup the Intel RealSense LiDAR L515 camera following the `Install RealSense2 - ROS <https://hagenmek.gitlab.io/mas514/src/guides.html#install-realsense2-ros>`__ guide
- Install and setup laser scanning from pointcloud using the `PointCloud to LaserScan <https://hagenmek.gitlab.io/mas514/src/guides.html#pointclound-to-laserscan>`__ guide





