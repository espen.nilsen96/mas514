Mapping
===============
The map can be a saved map or a new built map. However, the navigation stack does not require a map to run.

Building a new map
--------------------
The ROS node **slam_gmapping** provides the SLAM (Simultaneous Localization and Mapping) method which enables the JetBot to build a map while keeping track of its own position within that map.

Following the guide `How to Build a Map Using Logged Data <http://wiki.ros.org/slam_gmapping/Tutorials/MappingFromLoggedData>`__. Create a new bag file from laser scan data and use **slam_gmapping** to create a custom map adapted to the desired environment. The map is saved to disk using the **map_server** package with **map_saver**, further described in the guide mentioned above. Once saved, the map is available to be use in ROS.
