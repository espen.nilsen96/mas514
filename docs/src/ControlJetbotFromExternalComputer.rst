Wi-Fi JetBot Control
========================================

Follow the guide in `3.1.6. Network Configuration <https://emanual.robotis.com/docs/en/platform/turtlebot3/quick-start/#pc-setup>`__ to assign a remote master computer controlling the JetBot host.

It is also possible to control the JetBot from a Windows computer through a hotspot. This is done by sharing Windows Wi-Fi and using the remote SSH (Secure Shell) extension in Visual Studio Code, as further described in the `Getting Started <https://hagenmek.gitlab.io/mas514/src/start.html#>`__ guide.
