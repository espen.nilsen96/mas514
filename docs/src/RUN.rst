RUN
================


JetBot
-------------------
- Connect Arduino Nano and Intel RealSense L515 LiDAR camera
- Connect keyboard, mouse and monitor
- Connect the JetBot to power
- Logg in with the password ``jetbot`` 
- Connect remote computer through Wi-Fi or hotspot


From GitLab Repository
.........................

- Download https://gitlab.com/espen.nilsen96/mas514/-/tree/A2021/catkin_ws
- ``cd ~/catkin_ws`` 
- ``catkin_make``



Arduino
.........................
Open Arduino application

- Tools -> Board -> **Arduino Nano**
- Tools -> Port -> **/dev/ttyUSBO**
- Verify and upload the **wheelEn** code, found in ``catkin_ws/Arduino/wheelEn``






Autonomous Navigation
--------------------------
Initiating autonomous navigation in ROS Rviz:

- ``roslaunch mas514 start.launch``

New terminal.

- ``roslaunch mas514 move_base.launch``

New Terminal - enable keyboard control of the JetBot movement if needed:

- ``rosrun teleop_twist_keyboard teleop_twist_keyboard.py``


**Add** the following topics in Rviz:

- **TF** - shows all the frames, deselect all and select **camera_link** to follow JetBot
- **Map** - displays the map by selecting topic: **/map**

Order the robot to location:

- From top task bar select **2D Pose Estimate**, click and drag to set start position and orientation
- From top task bar select **2D Nav Goal**, click and drag to set destination position and orientation






