Navigation Stack
============================

The navigation stack requires a specific configuration to run on the JetBot. In the figure illustrated bellow, the white required components and the gray optional components are already implemented, while the blue components need to be configured specifically for the JetBot.



.. figure:: ../figs/overviewTF.png



Follow the `Setup and Configuration of the Navigation Stack on a Robot <http://wiki.ros.org/navigation/Tutorials/RobotSetup>`__ guide to create a base setup. Custom changes are then done to the codes described in this chapter.



Create a Package
-------------------------
To store all the configuration and launch files there has to be made a package with the necessary dependencies to run the navigation stack on the JetBot.
After finding a location for the package run:

.. literalinclude:: ../figs/catkin_create_pkg my_robot_name_2dnav mo.cpp
    :language: c++  



Robot Configuration
--------------------------
The **start.launch** file is the main configuration and launch file which includes all the packages, nodes and frame transformations the JetBot needs.


.. literalinclude:: ../figs/start.launch
    :language: c++  



Costmap Configuration
---------------------------

The **costmap_common_params.yaml** file stores information about obstacles to perform global and local planning. 
The common parameters set the threshold for obstacle response, defines the size of the robot (footprint or radius) and defines sensors.


.. literalinclude:: ../figs/costmap_common_params.yaml
    :language: c++





Base Local Planner Configuration
--------------------------------

The **base_local_planner.yaml** file defines the velocity and acceleration commands based on the JetBot specs. The statement **holonomic_robot: false**, is set because the JetBot is a robot with nonholonomic constraints. Nonholonomic constraints means that the robot is not free to move instantaneously in every direction within its degrees of freedom. 


.. literalinclude:: ../figs/costmap_common_params.yaml
    :language: c++ 




Launch Navigation Stack
-----------------------

The **move_base.launch** file is the launch file for the navigation stack which brings everything together to start navigation.

.. literalinclude:: ../figs/move_base.launch
    :language: c++  








